package com.example.taskinstagram;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HorizontalRVHolder> {


    Context context;
    ArrayList<HomeModel> arrayList;

    public HomeAdapter(){
        this.context=context;
        this.arrayList=arrayList;
    }

    @NonNull
    @Override
    public HorizontalRVHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_horizontal,viewGroup,false);
       return new HorizontalRVHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalRVHolder horizontalRVHolder, int i) {
        final HomeModel  homeModel= arrayList.get(i);
        horizontalRVHolder.textView.setText(homeModel.getName());
        horizontalRVHolder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(context,homeModel.getName(),Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class HorizontalRVHolder extends RecyclerView.ViewHolder {

        TextView textView;
        CircleImageView imageView;
        public HorizontalRVHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.horizontalcircleimage);
            textView=itemView.findViewById(R.id.horizontaltext);
        }
    }
}
