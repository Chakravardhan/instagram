package com.example.taskinstagram;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class HomeVerticalAdapter extends RecyclerView.Adapter<HomeVerticalAdapter.HomeVerticalHolder> {

    private Context context;
    ArrayList<HomeVerticalModel> arrayList;
    public  HomeVerticalAdapter()
    {
        this.context=context;
        this.arrayList=arrayList;
    }
    @NonNull
    @Override
    public HomeVerticalHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_vertical,viewGroup,false);
        return new HomeVerticalHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeVerticalHolder homeVerticalHolder, int i) {
        final  HomeVerticalModel homeVerticalModel=arrayList.get(i);
        homeVerticalModel.setVerticalImage(homeVerticalModel.getVerticalImage());
        homeVerticalHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, (CharSequence) homeVerticalHolder.itemView,Toast.LENGTH_LONG).show();
            }
        });
        homeVerticalHolder.btnfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, (CharSequence) homeVerticalModel.getBtnfav(),Toast.LENGTH_LONG).show();
            }
        });
        homeVerticalHolder.btnsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, (CharSequence) homeVerticalModel.getBtnsearch(),Toast.LENGTH_LONG).show();
            }
        });
        homeVerticalHolder.btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, (CharSequence) homeVerticalModel.getBtnsend(),Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class HomeVerticalHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        Button btnfav,btnsearch,btnsend;
        public HomeVerticalHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.verticalImage);
            btnfav=itemView.findViewById(R.id.verticalfav);
            btnsearch=itemView.findViewById(R.id.verticalsearch);
            btnsend=itemView.findViewById(R.id.verticalsend);
        }
    }
}
