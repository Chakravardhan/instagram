package com.example.taskinstagram;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity  implements  BottomNavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(this);

        navigation.setSelectedItemId(R.id.navigation_home);
    }
    Home home= new Home();
    Add add=new Add();
    Search search=new Search();
    Notification notification=new Notification();
    Block block= new Block();

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.containerfragment,home).commit();
                return true;
            case R.id.navigation_search:
                getSupportFragmentManager().beginTransaction().replace(R.id.containerfragment,search).commit();
                return true;

            case R.id.add_box:
                getSupportFragmentManager().beginTransaction().replace(R.id.containerfragment,add).commit();
                return  true;

            case R.id.navigation_notifications:
                getSupportFragmentManager().beginTransaction().replace(R.id.containerfragment,notification).commit();
                return true;
            case R.id.blockblack:
                getSupportFragmentManager().beginTransaction().replace(R.id.containerfragment,block).commit();
                return true;
        }
        return false;
    }

}
