package com.example.taskinstagram;

import android.widget.Button;
import android.widget.ImageView;

public class HomeVerticalModel {
    private  ImageView verticalImage;
    private  Button btnfav,btnsearch,btnsend;

    public HomeVerticalModel(ImageView verticalImage, Button btnfav, Button btnsearch, Button btnsend) {
        this.verticalImage = verticalImage;
        this.btnfav = btnfav;
        this.btnsearch = btnsearch;
        this.btnsend = btnsend;
    }

    public ImageView getVerticalImage() {
        return verticalImage;
    }

    public void setVerticalImage(ImageView verticalImage) {
        this.verticalImage = verticalImage;
    }

    public Button getBtnfav() {
        return btnfav;
    }

    public void setBtnfav(Button btnfav) {
        this.btnfav = btnfav;
    }

    public Button getBtnsearch() {
        return btnsearch;
    }

    public void setBtnsearch(Button btnsearch) {
        this.btnsearch = btnsearch;
    }

    public Button getBtnsend() {
        return btnsend;
    }

    public void setBtnsend(Button btnsend) {
        this.btnsend = btnsend;
    }
}
