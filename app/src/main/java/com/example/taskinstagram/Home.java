package com.example.taskinstagram;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment {


    public Home() {
        // Required empty public constructor
    }
    HomeAdapter mAdapter = new HomeAdapter();
    HomeVerticalAdapter homeVerticalAdapter=new HomeVerticalAdapter();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home, container, false);

        // Replace 'android.R.id.list' with the 'id' of your RecyclerView
        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.horizontalRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        Log.d("debugMode", "The application stopped after this");
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        // vertical Recycler View
        RecyclerView verticalrecyclerView=(RecyclerView) view.findViewById(R.id.verticalRecyclerView);
        RecyclerView.LayoutManager manager=new LinearLayoutManager(this.getActivity());
        verticalrecyclerView.setLayoutManager(manager);
        verticalrecyclerView.setAdapter(homeVerticalAdapter);

        return view;

    }

}
