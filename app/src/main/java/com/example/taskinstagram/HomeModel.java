package com.example.taskinstagram;

public class HomeModel {

    String name,descirption;

    public HomeModel(String name, String descirption) {
        this.name = name;
        this.descirption = descirption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescirption() {
        return descirption;
    }

    public void setDescirption(String descirption) {
        this.descirption = descirption;
    }
}
